<?
	include 'lib.php';
	$db = dbConnect();

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		//var_dump($_POST);


		$stmt = $db->prepare("REPLACE INTO games(user1_id, user2_id, score1, score2) VALUES(?,?,?,?)");
		foreach ($_POST['games'] as $id1 => $p1_games) {
			foreach ($p1_games as $id2 => $scores) {
				if ($scores['score1'] >= 0 && $scores['score2'] >= 0 && $scores['changed']) {
					$stmt->execute(array($id1, $id2, $scores['score1'], $scores['score2']));
				}
			}
		}		

		header('Location: '.$_SERVER['REQUEST_URI']);
		exit;
	}



	$games = getGames($db);
	$stats = getStats($db);



?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">

	<title>League</title>
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">


	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>


	<!-- Latest compiled and minified JavaScript -->
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

  <style>
  	body {
  		padding: 10px;
  	}

  	

  </style>

  <script>
  function trackChange(input) {
  	$(input).parent().nextAll('.change-tracker').val(1);
  }
  </script>


  
</head>
<body>

<h1>Заебище</h1>
<br/><br/>

<div class="row">
	<div class="games col-md-5">
		<form class="form-horizontal" role="form" method="post" action="<?=h($_SERVER['REQUEST_URI'])?>">
			<?foreach ($games as $game):?>
			<div class="form-group <?=($game['score1'] || $game['score2'] ? 'has-success' : '')?>">
    			<label class="col-xs-8"><?=h($game['user1_name'])?> - <?=h($game['user2_name'])?></label>
    			<div class="col-xs-2">
    				<input type="text" class="form-control" name="games[<?=$game['user1_id']?>][<?=$game['user2_id']?>][score1]" value="<?=$game['score1']?>" onchange="trackChange(this);">
    			</div>	
    			<div class="col-xs-2">
    				<input type="text" class="form-control" name="games[<?=$game['user1_id']?>][<?=$game['user2_id']?>][score2]" value="<?=$game['score2']?>" onchange="trackChange(this);">
    			</div>	

    			<input type="hidden" class="change-tracker" name="games[<?=$game['user1_id']?>][<?=$game['user2_id']?>][changed]" value="0">
  			</div>
			<?endforeach?>

			 <button type="submit" class="btn btn-success">Update</button>

		</form>	
	</div>

	<div class="stats col-md-5 pull-right">
		<table class="table">
  			<tr>
  				<th>pos</th>
  				<th>player</th>
  				<th>games</th>
  				<th>wins</th>
  				<th>score</th>
  			</tr>

  			<?foreach ($stats as $i => $item):?>
  			<tr>
  				<td><?=$i+1?></td>
  				<td><?=$item['name']?></td>
  				<td><?=$item['games_count']?></td>
  				<td><?=$item['wins_count']?></td>
  				<td><?=$item['score']?></td>
  			</tr>
  			<?endforeach?>

		</table>
	</div>



	
</div>	




</body>
</html>
