-- MySQL dump 10.13  Distrib 5.1.66, for apple-darwin12.2.0 (i386)
--
-- Host: localhost    Database: foosball
-- ------------------------------------------------------
-- Server version	5.1.66

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games` (
  `user1_id` int(11) NOT NULL DEFAULT '0',
  `user2_id` int(11) NOT NULL DEFAULT '0',
  `score1` int(11) DEFAULT NULL,
  `score2` int(11) DEFAULT NULL,
  PRIMARY KEY (`user1_id`,`user2_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES (1,2,0,0),(1,3,0,0),(1,4,0,0),(1,5,1,1),(1,6,0,0),(1,7,0,0),(1,8,0,2),(1,9,0,0),(1,10,0,0),(1,11,0,0),(2,3,0,0),(2,4,0,0),(2,5,1,1),(2,6,0,0),(2,7,0,0),(2,8,0,0),(2,9,1,1),(2,10,0,0),(2,11,0,0),(3,4,0,2),(3,5,0,2),(3,6,0,0),(3,7,0,0),(3,8,0,0),(3,9,1,1),(3,10,0,0),(3,11,0,0),(4,5,0,0),(4,6,0,0),(4,7,0,0),(4,8,0,2),(4,9,0,0),(4,10,0,0),(4,11,0,0),(5,6,0,0),(5,7,0,0),(5,8,0,0),(5,9,0,0),(5,10,0,0),(5,11,0,0),(6,7,0,0),(6,8,0,0),(6,9,0,2),(6,10,0,0),(6,11,0,0),(7,8,0,0),(7,9,0,0),(7,10,0,0),(7,11,0,0),(8,9,0,0),(8,10,0,0),(8,11,0,0),(9,10,0,0),(9,11,0,0),(10,11,0,0);
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Артем'),(2,'Богдан'),(3,'Димочка'),(4,'Люлька'),(5,'Женя'),(6,'Олег'),(7,'Макс'),(8,'Витя'),(9,'Саша'),(10,'БОРИСОВ'),(11,'Евгений');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-04-11 17:42:07
