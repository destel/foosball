<?

function dbConnect() {
	return new PDO('mysql:host=127.0.0.1;dbname=foosball;charset=utf8', 'foosball', 'foosball');
}

function getGames($db) {
	$sql = "
		SELECT u1.id as user1_id, u1.name as user1_name, u2.id as user2_id, u2.name as user2_name, g.score1, g.score2
		FROM users u1
		JOIN users u2 ON u2.id>u1.id
		LEFT JOIN games g ON u1.id=g.user1_id AND u2.id=g.user2_id
		ORDER BY (g.score1+g.score2 > 0) DESC, u1.id ASC, u2.id ASC
		";
	return $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);	
}

function getStats($db) {
	$sql = "
		SELECT
			user1_id as id,
			name, 
			COUNT(*) as games_count,
			SUM(score1 > score2) as wins_count,
			SUM(CASE 
			WHEN score1 > score2 THEN 3
			WHEN score1 = score2 THEN 1
			ELSE 0
		END) as score

			

		FROM (

		(SELECT u.name, g.user1_id, g.user2_id, g.score1, g.score2
		FROM users u
		JOIN games g ON u.id=g.user1_id AND (g.score1 <> 0 OR g.score2 <> 0))

		UNION

		(SELECT u.name, g.user2_id, g.user1_id , g.score2, g.score1
		FROM users u
		JOIN games g ON u.id=g.user2_id AND (g.score1 <> 0 OR g.score2 <> 0))
		) as q
		GROUP BY user1_id
		ORDER BY score DESC;

	";

	return $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);	
}


function h($x) {
	return htmlspecialchars($x);
}






?>
